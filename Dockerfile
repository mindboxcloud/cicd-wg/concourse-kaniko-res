# This file is a template, and might need editing before it works on your project.
FROM golang:1.8-alpine AS builder

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates
RUN go get github.com/GoogleContainerTools/kaniko/cmd/executor

FROM alpine:latest
RUN apk --no-cache add ca-certificates jq
COPY --from=builder /go/bin/executor /usr/bin/executor

# Since we started from scratch, we'll copy the SSL root certificates from the builder
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

COPY opt/resources /opt/resources
